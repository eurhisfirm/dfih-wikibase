# DFIH-Wikibase

_Wikibase bots for the "Données financières historiques (DFIH)" project_

## Install

```bash
git clone https://gitlab.huma-num.fr/eurhisfirm/dfih-wikibase.git
cd dfih-wikibase/
npm install
cp .env.template .env
```

Customize `.env` variables (see next sections for details).

## Register script as a MediaWiki bot

- go to the `Special:BotPasswords` special page of your Wikibase instance
- give the name `{username}@dfih-importer` to the bot for example (update `MEDIAWIKI_BOT_USERNAME` accordingly)
- check "Edit existing pages" and "Create, edit, and move pages" grants
- copy the generated password to `MEDIAWIKI_BOT_PASSWORD`

## Update Wikibase with DFIH issuers

```bash
node_modules/.bin/babel-node src/scripts/generate_wikibase_issuers.js
```
