import assert from "assert"
import levenshtein from "js-levenshtein"
import oracledb from "oracledb"

import config from "./config"
import {
  cleanUpLine,
  numericIdFromItemId,
  objectsFromSqlResult,
} from "./helpers"
import {
  getEndTimePropertyId,
  getOfPropertyId,
  getPositionHeldPropertyId,
  getStartTimePropertyId,
} from "./properties"

const corporateTitleItemIdById = {}

async function upsertCorporateTitle(connection, wikibase, job) {
  let corporateTitleItemId = corporateTitleItemIdById[job.id]
  if (corporateTitleItemId === undefined) {
    const item = await wikibase.upsertItemCore({
      aliases: [
        {
          language: "fr",
          value: job.short_name,
        },
      ],
      descriptions: [
        {
          language: "en",
          value: "Corporate title",
        },
        {
          language: "fr",
          value: "Titre d'entreprise",
        },
      ],
      labels: [
        {
          language: "fr",
          value: job.name,
        },
      ],
      sitelinks: [
        {
          site: config.wikibase.site,
          title: `jobs/${job.id}`,
        },
      ],
    })
    const claims = item.claims
    assert(claims !== undefined, JSON.stringify(item, null, 2))
    let lastrevid = item.lastrevid
    assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

    console.log(job.id, item.labels.fr.value, item.id)
    corporateTitleItemId = corporateTitleItemIdById[job.id] = item.id
  }
  return corporateTitleItemId
}

export async function upsertIssuerPersons(
  connection,
  wikibase,
  issuerId,
  issuerItemId,
) {
  console.log(`Loading persons for issuer ${issuerId} / ${issuerId}...`)
  const entries = objectsFromSqlResult(
    await connection.execute(
      `
        select
          p.id,
          p.name,
          to_char(pf.startdate, 'YYYY-MM-DD') as start_date,
          to_char(pf.enddate, 'YYYY-MM-DD') as end_date,
          j.id as job_id,
          j.name as job_name,
          j.shortname as job_short_name
        from person_function pf
        join person p on pf.person = p.id
        join jobs j on pf.job = j.id
        where pf.corporation = :issuerId
        order by p.id, pf.startdate, pf.enddate
      `,
      {
        issuerId: {
          dir: oracledb.BIND_IN,
          val: issuerId,
          type: oracledb.INTEGER,
        },
      },
    ),
  )
  const personById = {}
  for (let entry of entries) {
    let person = personById[entry.id]
    if (person === undefined) {
      person = personById[entry.id] = {
        id: entry.id,
        names: new Set(),
        functions: [],
      }
    }

    person.names.add(cleanUpLine(entry.name))

    const job = {
      id: entry.job_id,
      name: cleanUpLine(entry.job_name),
    }
    if (entry.job_short_name) {
      job.short_name = cleanUpLine(entry.job_short_name)
    }

    person.functions.push({
      job,
      start_date: entry.start_date,
      end_date: entry.end_date,
    })
  }

  for (let person of Object.values(personById)) {
    person.names = [...person.names].sort()

    // Compute best person name.
    let bestName = null
    let minDistance = Number.MAX_SAFE_INTEGER
    for (let name of person.names) {
      let distance = 0
      for (let otherName of person.names) {
        distance += levenshtein(name, otherName)
      }
      if (
        distance < minDistance ||
        (distance === minDistance && name.length < bestName.length)
      ) {
        bestName = name
        minDistance = distance
      }
    }
    if (bestName !== null) {
      person.name = bestName
    }
  }

  console.log("Updating or creating persons items...")
  const personItemIdById = {}
  for (let person of Object.values(personById)) {
    personItemIdById[person.id] = await upsertPerson(
      connection,
      wikibase,
      issuerItemId,
      person,
    )
  }

  return personItemIdById
}

export async function upsertPerson(connection, wikibase, issuerItemId, person) {
  const aliases = person.names
    .map(name => name.name)
    .filter(name => name !== person.name)
  const item = await wikibase.upsertItemCore({
    aliases: aliases
      .map(name => {
        return {
          language: "en",
          value: name,
        }
      })
      .concat(
        aliases.map(name => {
          return {
            language: "fr",
            value: name,
          }
        }),
      ),
    descriptions: [
      {
        language: "en",
        value: `Person (${person.id})`,
      },
      {
        language: "fr",
        value: `Personne (${person.id})`,
      },
    ],
    labels: [
      {
        language: "en",
        value: person.name,
      },
      {
        language: "fr",
        value: person.name,
      },
    ],
    sitelinks: [
      {
        site: config.wikibase.site,
        title: `persons/${person.id}`,
      },
    ],
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(person.id, item.labels.fr.value, item.id)

  {
    const endTimePropertyId = await getEndTimePropertyId(wikibase)
    const ofPropertyId = await getOfPropertyId(wikibase)
    const positionHeldPropertyId = await getPositionHeldPropertyId(wikibase)
    const startTimePropertyId = await getStartTimePropertyId(wikibase)

    const existingPositionHeldClaims = (
      claims[positionHeldPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datavalue.value["entity-type"] === "item" &&
        ((claim.qualifiers || {})[ofPropertyId] || []).some(
          qualifier =>
            qualifier.snaktype === "value" &&
            qualifier.datavalue.value["entity-type"] === "item" &&
            qualifier.datavalue.value.id === issuerItemId,
        ),
    )

    for (let personFunction of person.functions || []) {
      const job = personFunction.job
      const corporateTitleItemId = await upsertCorporateTitle(
        connection,
        wikibase,
        job,
      )
      const positionHeldClaimIndex = existingPositionHeldClaims.findIndex(
        claim =>
          claim.mainsnak.datavalue.value.id === corporateTitleItemId &&
          ((claim.qualifiers || {})[startTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${personFunction.start_date}T00:00:00Z`,
          ) &&
          ((claim.qualifiers || {})[endTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${personFunction.end_date}T00:00:00Z`,
          ),
      )
      if (positionHeldClaimIndex >= 0) {
        existingPositionHeldClaims.splice(positionHeldClaimIndex, 1)
      } else {
        // Create new position held claim.
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: positionHeldPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify({
            "entity-type": "item",
            "numeric-id": numericIdFromItemId(corporateTitleItemId),
          }),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        const positionHeldClaim = result.claim
        assert(positionHeldClaim !== undefined, JSON.stringify(result, null, 2))

        // Create "of issuer" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: positionHeldClaim.id,
          format: "json",
          property: ofPropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            "entity-type": "item",
            "numeric-id": numericIdFromItemId(issuerItemId),
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))

        // Create "start time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: positionHeldClaim.id,
          format: "json",
          property: startTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${personFunction.start_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))

        // Create "end time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: positionHeldClaim.id,
          format: "json",
          property: endTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${personFunction.end_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      }
    }

    if (existingPositionHeldClaims.length > 0) {
      const result = await wikibase.postToApi({
        action: "wbremoveclaims",
        baserevid: lastrevid,
        bot: true,
        claim: existingPositionHeldClaims.map(claim => claim.id).join("|"),
        format: "json",
        // summary: ...
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return item.id
}
