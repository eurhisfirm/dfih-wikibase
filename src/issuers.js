import assert from "assert"
import levenshtein from "js-levenshtein"
import oracledb from "oracledb"

import config from "./config"
import { cleanUpLine, objectsFromSqlResult } from "./helpers"
import { upsertIssuerPersons } from "./persons"

const issuerItemIdById = {}

export async function upsertIssuer(connection, wikibase, issuer) {
  const aliases = issuer.names
    .map(name => name.name)
    .filter(name => name !== issuer.name)
  const item = await wikibase.upsertItemCore({
    aliases: aliases
      .map(name => {
        return {
          language: "en",
          value: name,
        }
      })
      .concat(
        aliases.map(name => {
          return {
            language: "fr",
            value: name,
          }
        }),
      ),
    descriptions: [
      {
        language: "en",
        value: "Issuer",
      },
      {
        language: "fr",
        value: "Émetteur",
      },
    ],
    labels: [
      {
        language: "en",
        value: issuer.name,
      },
      {
        language: "fr",
        value: issuer.name,
      },
    ],
    sitelinks: [
      {
        site: config.wikibase.site,
        title: `issuers/${issuer.id}`,
      },
    ],
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(issuer.id, item.labels.fr.value, item.id)

  await upsertIssuerPersons(connection, wikibase, issuer.id, item.id)

  return item.id
}

export async function upsertIssuerId(connection, wikibase, issuerId) {
  let issuerItemId = issuerItemIdById[issuerId]
  if (issuerItemId === undefined) {
    const names = new Set()
    const issuer = {
      id: issuerId,
    }

    {
      // Add corporation_name.name to issuers names.
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              cn.name
            from corporation_name cn
            where cn.corporation = :issuerId
          `,
          {
            issuerId: {
              dir: oracledb.BIND_IN,
              val: issuerId,
              type: oracledb.INTEGER,
            },
          },
        ),
      )
      for (let entry of entries) {
        names.add(cleanUpLine(entry.name))
      }
    }
    {
      // Add corporation_true_name.truename to issuers names.
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              ctn.truename
            from corporation_true_name ctn
            where ctn.corporation = :issuerId
          `,
          {
            issuerId: {
              dir: oracledb.BIND_IN,
              val: issuerId,
              type: oracledb.INTEGER,
            },
          },
        ),
      )
      for (let entry of entries) {
        names.add(cleanUpLine(entry.truename))
      }
    }
    // Convert issuer names from a set to a sorted array and compute best names.
    {
      let bestName = null
      let minDistance = Number.MAX_SAFE_INTEGER
      issuer.names = [...names].sort()
      for (let name of issuer.names) {
        let distance = 0
        for (let otherName of issuer.names) {
          distance += levenshtein(name, otherName)
        }
        if (
          distance < minDistance ||
          (distance === minDistance && name.length < bestName.length)
        ) {
          bestName = name
          minDistance = distance
        }
      }
      issuer.name = bestName
    }

    issuerItemId = issuerItemIdById[issuerId] = await upsertIssuer(
      connection,
      wikibase,
      issuer,
    )
  }
  return issuerItemId
}

export async function upsertIssuers(connection, wikibase, { firstId = null }) {
  console.log("Updating or creating issuers items...")
  // Add corporation_name.name to issuers names.
  const issuersIds = objectsFromSqlResult(
    await connection.execute(
      `
        select
          distinct cn.corporation,
        from corporation_name cn
        where cn.name not like '%PSE%'
        order by id
      `,
    ),
  ).map(entry => entry.id)

  let skip = !!firstId
  for (let issuerId of issuersIds) {
    if (skip) {
      if (issuerId === firstId) {
        skip = false
      } else {
        continue
      }
    }
    await upsertIssuerId(connection, wikibase, issuerId)
  }
}
