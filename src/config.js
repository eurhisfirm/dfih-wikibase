require("dotenv").config()

import { validateConfig } from "./validators/config"

const config = {
  database: {
    connectString: process.env.DATABASE_CONNECT_STRING || "ORACLE_SERVER_DOMAIN_NAME/dbdfih",
    user: process.env.DATABASE_USERNAME || "DATABASE_USERNAME",
    password: process.env.DATABASE_PASSWORD || "DATABASE_PASSWORD",
  },
  wikibase: {
    url: process.env.WIKIBASE_URL || "https://wiki.dfih.fr/",
    user: process.env.MEDIAWIKI_BOT_USERNAME || "MEDIAWIKI_BOT_USERNAME",
    password: process.env.MEDIAWIKI_BOT_PASSWORD || "MEDIAWIKI_BOT_USERNAME",
    site: process.env.MEDIAWIKI_SITELINK_SITE || "dfih.fr",
  },
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`
  )
  process.exit(-1)
}

export default validConfig
