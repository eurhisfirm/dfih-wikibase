import assert from "assert"
import oracledb from "oracledb"

import { numericIdFromItemId, objectsFromSqlResult } from "./helpers"
import { getSecurityItemId } from "./items"
import { getSubclassOfPropertyId } from "./properties"

const stockTypeSummaryById = {}

export async function upsertStockType(connection, wikibase, stockType) {
  const item = await wikibase.upsertItemCore({
    descriptions: [
      {
        language: "en",
        value: "security type",
      },
      {
        language: "fr",
        value: "type de valeur mobilière",
      },
    ],
    labels: [
      {
        language: "en",
        value: stockType.name,
      },
      {
        language: "fr",
        value: stockType.name,
      },
    ],
    // sitelinks: [{
    //   site: config.wikibase.site,
    //   title: `stock-types/${stockType.id}`,
    // }],
  })
  const claims = item.claims
  assert.notStrictEqual(claims, undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert.notStrictEqual(lastrevid, undefined, JSON.stringify(item, null, 2))

  console.log(stockType.id, item.labels.fr.value, item.id)

  {
    const securityItemId = await getSecurityItemId(wikibase)
    const subclassOfPropertyId = await getSubclassOfPropertyId(wikibase)

    const existingSubclassOfSecurityClaims = (
      claims[subclassOfPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datavalue.value["entity-type"] === "item" &&
        claim.mainsnak.datavalue.value.id === securityItemId,
    )
    if (existingSubclassOfSecurityClaims.length === 0) {
      // Create a new claim "item is subclass of security".
      let result = await wikibase.postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: item.id,
        format: "json",
        property: subclassOfPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify({
          "entity-type": "item",
          "numeric-id": numericIdFromItemId(securityItemId),
        }),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      const subclassOfSecurityClaim = result.claim
      assert(
        subclassOfSecurityClaim !== undefined,
        JSON.stringify(result, null, 2),
      )
    }
  }

  return item.id
}

export async function upsertStockTypeId(connection, wikibase, stockTypeId) {
  let stockTypeSummary = stockTypeSummaryById[stockTypeId]
  if (stockTypeSummary === undefined) {
    const stockType = objectsFromSqlResult(
      await connection.execute(
        `
          select
            st.id,
            st.name,
            st.stock_group
          from stocktype st
          where st.id = :stockTypeId
        `,
        {
          stockTypeId: {
            dir: oracledb.BIND_IN,
            val: stockTypeId,
            type: oracledb.INTEGER,
          },
        },
      ),
    )[0]
    const stockTypeItemId = await upsertStockType(
      connection,
      wikibase,
      stockType,
    )
    stockTypeSummary = stockTypeSummaryById[stockTypeId] = {
      itemId: stockTypeItemId,
      name: stockType.name,
    }
  }
  return stockTypeSummary
}

export async function upsertStockTypes(connection, wikibase) {
  console.log("Loading stock types...")
  const stocksTypesIds = objectsFromSqlResult(
    await connection
      .execute(
        `
        select
          st.id
        from stocktype st
        where
          st.id <> 1
          or st.name = 'UNKNOWN'
      `,
      )
      .map(entry => entry.id),
  )

  for (let stockTypeId of stocksTypesIds) {
    await upsertStockTypeId(connection, wikibase, stockTypeId)
  }
}
