let endTimePropertyId = null
let imagePropertyId = null
let instanceOfPropertyId = null
let listedOnPropertyId = null
let ofPropertyId = null
let positionHeldPropertyId = null
let securityOfPropertyId = null
let startTimePropertyId = null
let subclassOfPropertyId = null

export async function getEndTimePropertyId(wikibase) {
  if (endTimePropertyId === null) {
    endTimePropertyId = (await wikibase.upsertPropertyCore({
      datatype: "time",
      descriptions: [
        {
          language: "en",
          value:
            "indicates the time an item ceases to exist or a statement stops being valid",
        },
        {
          language: "fr",
          value: "indique la date de fin de validité",
        },
      ],
      labels: [
        {
          language: "en",
          value: "end time",
        },
        {
          language: "fr",
          value: "date de fin",
        },
      ],
    })).id
    console.log("end time property: ", endTimePropertyId)
  }
  return endTimePropertyId
}

export async function getImagePropertyId(wikibase) {
  if (imagePropertyId === null) {
    imagePropertyId = (await wikibase.upsertPropertyCore({
      datatype: "TODO",
      descriptions: [
        {
          language: "en",
          value:
            "image of relevant illustration of the subject; if available, use more specific properties",
        },
        {
          language: "fr",
          value:
            "image illustrant l'élément ; voir page de discussion pour d'autres propriétés",
        },
      ],
      labels: [
        {
          language: "en",
          value: "image",
        },
        {
          language: "fr",
          value: "image",
        },
      ],
    })).id
    console.log("image property: ", imagePropertyId)
  }
  return imagePropertyId
}

export async function getInstanceOfPropertyId(wikibase) {
  if (instanceOfPropertyId === null) {
    instanceOfPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value:
            "that class of which this subject is a particular example and member (subject typically an individual" +
            " member with a proper name label)",
        },
        {
          language: "fr",
          value:
            "cet élément est un exemple spécifique de cette classe qui en précise la nature",
        },
      ],
      labels: [
        {
          language: "en",
          value: "instance of",
        },
        {
          language: "fr",
          value: "nature de l'élément",
        },
        // sitelinks: [{
        //   site: "wikidata",
        //   title: "P31",
        // }],
      ],
    })).id
    console.log("instance of property: ", instanceOfPropertyId)
  }
  return instanceOfPropertyId
}

export async function getListedOnPropertyId(wikibase) {
  if (listedOnPropertyId === null) {
    listedOnPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value: "stock exchange on which the subject security is listed",
        },
        {
          language: "fr",
          value: "bourse à laquelle le titre mobilier est inscrit",
        },
      ],
      labels: [
        {
          language: "en",
          value: "listed on",
        },
        {
          language: "fr",
          value: "inscrit à",
        },
        // sitelinks: [{
        //   site: "wikidata",
        //   title: "???",
        // }],
      ],
    })).id
    console.log("listed on property: ", listedOnPropertyId)
  }
  return listedOnPropertyId
}

export async function getOfPropertyId(wikibase) {
  if (ofPropertyId === null) {
    ofPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value:
            "qualifier stating that a statement applies within the scope of a particular item",
        },
        {
          language: "fr",
          value:
            "qualificateur indiquant que la déclaration s'applique dans l'étendue d'un élément particulier",
        },
      ],
      labels: [
        {
          language: "en",
          value: "of",
        },
        {
          language: "fr",
          value: "de",
        },
      ],
    })).id
    console.log("of property:", ofPropertyId)
  }
  return ofPropertyId
}

export async function getPositionHeldPropertyId(wikibase) {
  if (positionHeldPropertyId === null) {
    positionHeldPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value:
            "subject currently or formerly holds the object position or public office",
        },
        {
          language: "fr",
          value: "fonction occupée (politique, ecclésiastique, etc.)",
        },
      ],
      labels: [
        {
          language: "en",
          value: "position held",
        },
        {
          language: "fr",
          value: "fonction",
        },
      ],
    })).id
    console.log("position held property: ", positionHeldPropertyId)
  }
  return positionHeldPropertyId
}

export async function getSecurityOfPropertyId(wikibase) {
  if (securityOfPropertyId === null) {
    securityOfPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value: "company owned by the security",
        },
        {
          language: "fr",
          value: "entreprise à laquelle correspond le titre mobilier",
        },
      ],
      labels: [
        {
          language: "en",
          value: "security of",
        },
        {
          language: "fr",
          value: "titre mobilier de",
        },
        // sitelinks: [{
        //   site: "wikidata",
        //   title: "???",
        // }],
      ],
    })).id
    console.log("listed on property: ", securityOfPropertyId)
  }
  return securityOfPropertyId
}

export async function getStartTimePropertyId(wikibase) {
  if (startTimePropertyId === null) {
    startTimePropertyId = (await wikibase.upsertPropertyCore({
      datatype: "time",
      descriptions: [
        {
          language: "en",
          value: "indicates the time a statement starts being valid",
        },
        {
          language: "fr",
          value:
            "qualificateur indiquant la date de début de validité pour une déclaration",
        },
      ],
      labels: [
        {
          language: "en",
          value: "start time",
        },
        {
          language: "fr",
          value: "date de début",
        },
      ],
    })).id
    console.log("start time property: ", startTimePropertyId)
  }
  return startTimePropertyId
}

export async function getSubclassOfPropertyId(wikibase) {
  if (subclassOfPropertyId === null) {
    subclassOfPropertyId = (await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value:
            "all instances of these items are instances of those items; this item is a class (subset) of that" +
            " item",
        },
        {
          language: "fr",
          value:
            "X sous-classe de Y : tous les éléments qui sont des instances (exemples) de X sont aussi des" +
            " éléments instances de Y",
        },
      ],
      labels: [
        {
          language: "en",
          value: "subclass of",
        },
        {
          language: "fr",
          value: "sous-classe de",
        },
        // sitelinks: [{
        //   site: "wikidata",
        //   title: "P279",
        // }],
      ],
    })).id
    console.log("subclass of property: ", subclassOfPropertyId)
  }
  return subclassOfPropertyId
}
