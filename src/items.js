let securityItemId = null

export async function getSecurityItemId(wikibase) {
  if (securityItemId === null) {
    securityItemId = (await wikibase.upsertItemCore({
      descriptions: [
        {
          language: "en",
          value: "tradable financial asset",
        },
        {
          language: "fr",
          value: "denrée exportable dans le secteur financier",
        },
      ],
      labels: [
        {
          language: "en",
          value: "security",
        },
        {
          language: "fr",
          value: "valeur mobilière",
        },
      ],
      sitelinks: [
        {
          site: "enwiki",
          title: "Security (finance)",
        },
        //   {
        //     site: "wikidata",
        //     title: "Q169489",
        //   },
      ],
    })).id
    console.log("security item:", securityItemId)
  }
  return securityItemId
}
