import {
  assertValid,
  validateItemCore,
  validatePropertyCore,
} from "@biryani/entities"
import assert from "assert"
import url from "url"

import { sleep } from "./helpers"

export default class Wikibase {
  constructor({ password, request, url, user }) {
    this.csrfToken = null
    this.password = password
    this.request = request
    this.url = url
    this.user = user
  }

  async getEntity(entityType, data) {
    const firstLabel = { ...data.labels[0] }

    let entity = null
    let result = null
    if (data.sitelinks === undefined) {
      result = await this.postToApi({
        action: "wbsearchentities",
        format: "json",
        language: firstLabel.language,
        limit: 10,
        search: firstLabel.value,
        type: entityType,
      })
      const labelsLowerCase = data.labels.map(label => {
        return {
          ...label,
          value: label.value.toLowerCase(),
        }
      })
      // Caution: The `description` and `label` fields in result.search are in the defaut language (and not in
      // the POSTed language).
      const searchResults = result.search.filter(
        ({ description: foundDescription, label: foundLabel }) => {
          if (entityType === "item") {
            return data.labels.some(({ language, value }) => {
              if (value !== foundLabel) {
                return false
              }
              const description =
                data.descriptions === undefined
                  ? undefined
                  : data.descriptions.filter(
                      description => description.language === language,
                    )[0]
              return (
                (description === undefined && foundDescription === undefined) ||
                (description !== undefined &&
                  description.value === foundDescription)
              )
            })
          } else {
            assert.strictEqual(entityType, "property")
            // There can only be a single property with the same label (label casing & description are not used ).
            const foundLabelLowerCase = foundLabel.toLowerCase()
            return labelsLowerCase.some(
              ({ value }) => value === foundLabelLowerCase,
            )
          }
        },
      )
      if (searchResults.length > 0) {
        result = await this.postToApi({
          action: "wbgetentities",
          format: "json",
          ids: searchResults[0].id,
        })
        assert(result.entities)
        assert(Object.keys(result.entities).length === 1)
        entity = Object.values(result.entities)[0]
      }
    } else {
      const firstSiteLink = data.sitelinks[0]
      result = await this.postToApi({
        action: "wbgetentities",
        format: "json",
        sites: firstSiteLink.site,
        titles: firstSiteLink.title,
      })
      assert(result.entities)
      assert(Object.keys(result.entities).length === 1)
      entity = Object.values(result.entities)[0]
    }
    return entity
  }

  async getItem(data) {
    data = assertValid(validateItemCore(data))
    return await this.getEntity("item", data)
  }

  async getProperty(data) {
    data = assertValid(validatePropertyCore(data))
    return await this.getEntity("property", data)
  }

  async login() {
    let result = await this.request.post(url.resolve(this.url, "api.php"), {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
        type: "login",
      },
      json: true,
    })
    assert(result.error === undefined, JSON.stringify(result, null, 2))
    // console.log(JSON.stringify(result, null, 2))
    const loginToken = result.query.tokens.logintoken

    result = await this.request.post(url.resolve(this.url, "api.php"), {
      form: {
        action: "login",
        format: "json",
        lgname: this.user,
        lgpassword: this.password,
        lgtoken: loginToken,
      },
      json: true,
    })
    assert(result.error === undefined, JSON.stringify(result, null, 2))
    // console.log(JSON.stringify(result, null, 2))
  }

  async postToApi(body) {
    let result = null
    for (let delay of [
      1,
      2,
      4,
      8,
      16,
      32,
      64,
      128,
      256,
      512,
      1024,
      2048,
      4096,
      8192,
      16384,
      0,
    ]) {
      const form = {
        ...body,
        token: this.csrfToken,
      }
      result = await this.request.post(url.resolve(this.url, "api.php"), {
        form,
        json: true,
      })
      if (result.error === undefined) {
        return result
      }
      if (
        result.error.messages &&
        result.error.messages.some(
          message =>
            message.name ===
            "wikibase-validator-label-with-description-conflict",
        )
      ) {
        // Duplicate label: Let the caller handle this problem.
        return result
      }
      if (result.error.code === "badtoken") {
        // await logout()
        await this.login()
        await this.requestCsrfToken()
        continue
      }
      if (
        result.error.code === "failed-save" &&
        result.error.messages &&
        result.error.messages.some(
          message => message.name === "actionthrottledtext",
        )
      ) {
        console.log(
          "A failed-save error occurred:",
          JSON.stringify(result, null, 2),
        )
        console.log(`Sleeping ${delay} seconds...`)
        sleep(delay)
        continue
      }
      if (result.error.code === "no-external-page") {
        console.log(
          "A no-external-page error occurred:",
          JSON.stringify(result, null, 2),
        )
        console.log(`Sleeping ${delay} seconds...`)
        sleep(delay)
        continue
      }
      // Unhandled error. Throw an exception.
      break
    }
    assert(result.error === undefined, JSON.stringify(result, null, 2))
  }

  async requestCsrfToken() {
    const result = await this.request.post(url.resolve(this.url, "api.php"), {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
      },
      json: true,
    })
    assert(result.error === undefined, JSON.stringify(result, null, 2))
    this.csrfToken = result.query.tokens.csrftoken
    assert(this.csrfToken !== undefined, JSON.stringify(result, null, 2))
    return this.csrfToken
  }

  // Update or create an item core (without claims).
  async upsertItemCore(data) {
    data = assertValid(validateItemCore(data))
    return await this.upsertValidEntityCore("item", data)
  }

  async upsertPropertyCore(data) {
    data = assertValid(validatePropertyCore(data))
    return await this.upsertValidEntityCore("property", data)
  }

  async upsertValidEntityCore(entityType, data) {
    const entity = await this.getEntity(entityType, data)
    let result = null
    if (entity === null || entity.missing !== undefined) {
      // Wikibase entity is missing. Create it.
      result = await this.postToApi({
        action: "wbeditentity",
        data: JSON.stringify(data, null, 2),
        format: "json",
        new: entityType,
      })
      if (
        result.error &&
        result.error.messages &&
        result.error.messages.some(
          message =>
            message.name ===
            "wikibase-validator-label-with-description-conflict",
        )
      ) {
        throw `Error while creating entity. An entity with the same name already exists:\n${JSON.stringify(
          data,
          null,
          2,
        )}\n${JSON.stringify(result, null, 2)}`
      }
      entity = result.entity
    } else {
      // Check if existing entity needs to be updated.
      const dataChanges = {}
      if (data.aliases !== undefined) {
        if (
          data.aliases.some(dataAlias => {
            const entityAliases = entity.aliases[dataAlias.language]
            if (entityAliases === undefined) {
              return true
            }
            return !entityAliases.some(
              entityAlias => entityAlias.value === dataAlias.value,
            )
          })
        ) {
          // Some aliases in data are not present in entity. Add them all.
          dataChanges.aliases = data.aliases.map(alias => {
            alias.add = ""
            return alias
          })
        }
      }
      if (data.datatype !== undefined) {
        assert.strictEqual(
          data.datatype,
          entity.datatype,
          `Existing property ${JSON.stringify(entity, null, 2)} has datatype "${
            entity.datatype
          }" different from "${data.datatype}"`,
        )
      }
      if (data.descriptions !== undefined) {
        if (
          data.descriptions.some(description => {
            const entityDescription = entity.descriptions[description.language]
            return (
              entityDescription === undefined ||
              description.value !== entityDescription.value
            )
          })
        ) {
          // Some descriptions in data are missing from entity. Add them.
          dataChanges.descriptions = data.descriptions
        }
      }
      if (
        data.labels.some(label => {
          const entityLabel = entity.labels[label.language]
          return entityLabel === undefined || label.value !== entityLabel.value
        })
      ) {
        // Some labels in data are missing from entity. Add them.
        dataChanges.labels = data.labels
      }
      if (data.sitelinks !== undefined) {
        if (
          data.sitelinks.some(siteLink => {
            const entitySiteLink = entity.sitelinks[siteLink.site]
            return (
              entitySiteLink === undefined ||
              siteLink.title !== entitySiteLink.title
            )
          })
        ) {
          // Some sitelinks in data are missing from entity. Add them.
          dataChanges.sitelinks = data.sitelinks
        }
      }
      if (Object.keys(dataChanges).length > 0) {
        // Update entity.
        console.log(
          `Updating entity ${entity.id}:`,
          JSON.stringify(dataChanges, null, 2),
        )
        result = await this.postToApi({
          action: "wbeditentity",
          data: JSON.stringify(dataChanges, null, 2),
          format: "json",
          id: entity.id,
        })
        if (
          result.error &&
          result.error.messages &&
          result.error.messages.some(
            message =>
              message.name ===
              "wikibase-validator-label-with-description-conflict",
          )
        ) {
          throw `Error while updating entity ${
            entity.id
          }. An entity with the same name already exists:\n${JSON.stringify(
            data,
            null,
            2,
          )}\n${JSON.stringify(result, null, 2)}`
        }
        entity = result.entity
      }
    }
    return entity
  }
}
