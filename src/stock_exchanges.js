import assert from "assert"
import levenshtein from "js-levenshtein"
import oracledb from "oracledb"

import config from "./config"
import { cleanUpLine, objectsFromSqlResult } from "./helpers"

const stockExchangeItemIdById = {}

export async function upsertStockExchange(connection, wikibase, stockExchange) {
  const aliases = stockExchange.names
    .map(name => name.name)
    .filter(name => name !== stockExchange.name)
  const item = await wikibase.upsertItemCore({
    aliases: aliases
      .map(name => {
        return {
          language: "fr",
          value: name,
        }
      })
      .concat(
        aliases.map(name => {
          return {
            language: "fr",
            value: name,
          }
        }),
      ),
    descriptions: [
      {
        language: "en",
        value: "Stock exchange",
      },
      {
        language: "fr",
        value: "Bourse",
      },
    ],
    labels: [
      {
        language: "en",
        value: stockExchange.name,
      },
      {
        language: "fr",
        value: stockExchange.name,
      },
    ],
    sitelinks: [
      {
        site: config.wikibase.site,
        title: `stock-exchanges/${stockExchange.id}`,
      },
    ],
  })
  const claims = item.claims
  assert.notStrictEqual(claims, undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert.notStrictEqual(lastrevid, undefined, JSON.stringify(item, null, 2))

  console.log(stockExchange.id, item.labels.fr.value, item.id)

  return item.id
}

export async function upsertStockExchangeId(
  connection,
  wikibase,
  stockExchangeId,
) {
  let stockExchangeItemId = stockExchangeItemIdById[stockExchangeId]
  if (stockExchangeItemId === undefined) {
    const stockExchange = {
      id: stockExchangeId,
    }
    stockExchange.names = objectsFromSqlResult(
      await connection.execute(
        `
          select
            sen.name,
            to_char(sen.startdate, 'YYYY-MM-DD') as start_date,
            to_char(sen.enddate, 'YYYY-MM-DD') as end_date
          from stockexchange_name sen
          where sen.stockexchange = :stockExchangeId
          order by sen.startdate, sen.enddate
        `,
        {
          stockExchangeId: {
            dir: oracledb.BIND_IN,
            val: stockExchangeId,
            type: oracledb.INTEGER,
          },
        },
      ),
    ).map(entry => {
      return {
        name: cleanUpLine(entry.name),
        start_date: entry.start_date,
        end_date: entry.end_date,
      }
    })

    {
      // Compute best name for stock exchange.
      let bestName = null
      let minDistance = Number.MAX_SAFE_INTEGER
      let names = [
        ...new Set(stockExchange.names.map(name => name.name)),
      ].sort()
      for (let name of names) {
        let distance = 0
        for (let otherName of names) {
          distance += levenshtein(name, otherName)
        }
        if (
          distance < minDistance ||
          (distance === minDistance && name.length < bestName.length)
        ) {
          bestName = name
          minDistance = distance
        }
      }
      stockExchange.name = bestName
    }

    stockExchangeItemId = stockExchangeItemIdById[
      stockExchangeId
    ] = await upsertStockExchange(connection, wikibase, stockExchange)
  }
  return stockExchangeItemId
}

export async function upsertStockExchanges(connection, wikibase) {
  console.log("Loading stock exchanges...")
  const stockExchangesIds = [1, 2]

  for (let stockExchangeId of stockExchangesIds) {
    await upsertStockExchangeId(connection, wikibase, stockExchangeId)
  }
}
