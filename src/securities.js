import assert from "assert"
import levenshtein from "js-levenshtein"
import oracledb from "oracledb"

import config from "./config"
import {
  cleanUpLine,
  numericIdFromItemId,
  objectsFromSqlResult,
} from "./helpers"
import { upsertIssuerId } from "./issuers"
import {
  getEndTimePropertyId,
  getInstanceOfPropertyId,
  getSecurityOfPropertyId,
  getStartTimePropertyId,
  // getOfPropertyId,
} from "./properties"
import { upsertStockTypeId } from "./stock_types"

const securityItemIdById = {}

export async function upsertSecurities(
  connection,
  wikibase,
  { firstId = null },
) {
  console.log("Updating or creating securities items...")
  const securitiesIds = objectsFromSqlResult(
    await connection.execute(
      `
        select
          s.id
        from stock s
        where
          not exists (select null from notation n where n.stock = s.id and n.sector = 75)
          and exists (select null from stock_name sn where sn.stock = s.id)
        order by s.id
      `,
    ),
  ).reduce((accumulator, { id }) => {
    accumulator.push(id)
    return accumulator
  }, [])

  let skip = !!firstId
  for (let securityId of securitiesIds) {
    if (skip) {
      if (securityId === firstId) {
        skip = false
      } else {
        continue
      }
    }
    await upsertSecurityId(connection, wikibase, securityId)
  }
}

export async function upsertSecurity(connection, wikibase, security) {
  let names = new Set()
  for (let notationById of Object.values(
    security.notationByIdByStockExchangeId,
  )) {
    for (let notation of Object.values(notationById)) {
      for (let name of notation.names) {
        names.add(name.name)
      }
    }
  }
  names = [...names].sort()

  const typeName =
    security.types.length > 0
      ? security.types[security.types.length - 1].stockTypeSummary.name
      : "valeur mobilière"
  const item = await wikibase.upsertItemCore({
    aliases: names.map(name => {
      return {
        language: "fr",
        value: name,
      }
    }),
    descriptions: [
      {
        language: "fr",
        value: `${typeName} (${security.id})`,
      },
    ],
    labels: [
      {
        language: "en",
        value: security.name || "Sans nom",
      },
      {
        language: "fr",
        value: security.name || "Sans nom",
      },
    ],
    sitelinks: [
      {
        site: config.wikibase.site,
        title: `securities/${security.id}`,
      },
    ],
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(security.id, item.labels.fr.value, item.id)

  // Upsert claims for "security is an instanceOf security type between two dates".
  {
    const endTimePropertyId = await getEndTimePropertyId(wikibase)
    const instanceOfPropertyId = await getInstanceOfPropertyId(wikibase)
    // const ofPropertyId = await getOfPropertyId(wikibase)
    const startTimePropertyId = await getStartTimePropertyId(wikibase)

    const existingInstanceOfClaims = (
      claims[instanceOfPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datavalue.value["entity-type"] === "item",
    )

    for (let securityType of security.types) {
      const stockTypeItemId = securityType.stockTypeSummary.itemId
      const instanceOfClaimIndex = existingInstanceOfClaims.findIndex(
        claim =>
          claim.mainsnak.datavalue.value.id === stockTypeItemId &&
          ((claim.qualifiers || {})[startTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${securityType.start_date}T00:00:00Z`,
          ) &&
          ((claim.qualifiers || {})[endTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${securityType.end_date}T00:00:00Z`,
          ),
      )
      if (instanceOfClaimIndex >= 0) {
        existingInstanceOfClaims.splice(instanceOfClaimIndex, 1)
      } else {
        // Create new instanceOf claim.
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: instanceOfPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify({
            "entity-type": "item",
            "numeric-id": numericIdFromItemId(stockTypeItemId),
          }),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        const instanceOfClaim = result.claim
        assert(instanceOfClaim !== undefined, JSON.stringify(result, null, 2))

        // Create "start time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: instanceOfClaim.id,
          format: "json",
          property: startTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${securityType.start_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))

        // Create "end time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: instanceOfClaim.id,
          format: "json",
          property: endTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${securityType.end_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))

        // // Create "stock group" qualifier.
        // result = await wikibase.postToApi({
        //   action: "wbsetqualifier",
        //   baserevid: lastrevid,
        //   bot: true,
        //   claim: instanceOfClaim.id,
        //   format: "json",
        //   property: ofPropertyId,
        //   snaktype: "value",
        //   // summary: "Adding qualifier",
        //   value: JSON.stringify({
        //     "entity-type": "item",
        //     "numeric-id": numericIdFromItemId(issuerItemId),
        //   }),
        // })
        // assert(result.error === undefined, JSON.stringify(result, null, 2))
        // // console.log(JSON.  stringify(result, null, 2))
        // lastrevid = result.pageinfo.lastrevid
        // assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      }
    }

    if (existingInstanceOfClaims.length > 0) {
      const result = await wikibase.postToApi({
        action: "wbremoveclaims",
        baserevid: lastrevid,
        bot: true,
        claim: existingInstanceOfClaims.map(claim => claim.id).join("|"),
        format: "json",
        // summary: ...
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
    }
  }

  // Upsert claims for "security is a securityOf issuer between two dates".
  {
    const endTimePropertyId = await getEndTimePropertyId(wikibase)
    const securityOfPropertyId = await getSecurityOfPropertyId(wikibase)
    // const ofPropertyId = await getOfPropertyId(wikibase)
    const startTimePropertyId = await getStartTimePropertyId(wikibase)

    const existingSecurityOfClaims = (
      claims[securityOfPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datavalue.value["entity-type"] === "item",
    )

    for (let issuer of security.issuers) {
      const securityOfClaimIndex = existingSecurityOfClaims.findIndex(
        claim =>
          claim.mainsnak.datavalue.value.id === issuer.itemId &&
          ((claim.qualifiers || {})[startTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${issuer.start_date}T00:00:00Z`,
          ) &&
          ((claim.qualifiers || {})[endTimePropertyId] || []).some(
            qualifier =>
              qualifier.snaktype === "value" &&
              qualifier.datavalue.value.precision === 11 && // day
              qualifier.datavalue.value.time ===
                `+${issuer.end_date}T00:00:00Z`,
          ),
      )
      if (securityOfClaimIndex >= 0) {
        existingSecurityOfClaims.splice(securityOfClaimIndex, 1)
      } else {
        // Create new securityOf claim.
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: securityOfPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify({
            "entity-type": "item",
            "numeric-id": numericIdFromItemId(issuer.itemId),
          }),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        const securityOfClaim = result.claim
        assert(securityOfClaim !== undefined, JSON.stringify(result, null, 2))

        // Create "start time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: securityOfClaim.id,
          format: "json",
          property: startTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${issuer.start_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))

        // Create "end time" qualifier.
        result = await wikibase.postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: securityOfClaim.id,
          format: "json",
          property: endTimePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify({
            after: 0,
            before: 0,
            calendarmodel: "http://www.wikidata.org/entity/Q1985727",
            precision: 11,
            time: `+${issuer.end_date}T00:00:00Z`,
            timezone: 0,
          }),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      }
    }

    if (existingSecurityOfClaims.length > 0) {
      const result = await wikibase.postToApi({
        action: "wbremoveclaims",
        baserevid: lastrevid,
        bot: true,
        claim: existingSecurityOfClaims.map(claim => claim.id).join("|"),
        format: "json",
        // summary: ...
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return item.id
}

export async function upsertSecurityId(connection, wikibase, securityId) {
  let securityItemId = securityItemIdById[securityId]
  if (securityItemId === undefined) {
    const security = {
      id: securityId,
    }

    {
      // Add issuers to security.
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              sc.corporation as id,
              to_char(sc.startdate, 'YYYY-MM-DD') as start_date,
              to_char(sc.enddate, 'YYYY-MM-DD') as end_date
            from stock_corporation sc
            where sc.stock = :securityId
          `,
          {
            securityId: {
              dir: oracledb.BIND_IN,
              val: securityId,
              type: oracledb.INTEGER,
            },
          },
        ),
      )
      const issuers = []
      for (let entry of entries) {
        const issuerItemId = await upsertIssuerId(
          connection,
          wikibase,
          entry.id,
        )
        issuers.push({
          ...entry,
          itemId: issuerItemId,
        })
      }
      security.issuers = issuers
    }

    {
      // Add types to security.
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              st.type as type_id,
              to_char(st.startdate, 'YYYY-MM-DD') as start_date,
              to_char(st.enddate, 'YYYY-MM-DD') as end_date
            from stock_type st
            where st.stock = :securityId
          `,
          {
            securityId: {
              dir: oracledb.BIND_IN,
              val: securityId,
              type: oracledb.INTEGER,
            },
          },
        ),
      )
      const types = []
      for (let entry of entries) {
        const stockTypeSummary = await upsertStockTypeId(
          connection,
          wikibase,
          entry.type_id,
        )
        delete entry.type_id
        types.push({
          ...entry,
          stockTypeSummary,
        })
      }
      security.types = types
    }

    {
      // Add "mainstock" flag to security.
      const entry = await connection.execute(
        `
          select count(distinct mainstock) as isMainstock
          from corporation_mainstocks cm
          where cm.mainstock = :securityId
          `,
        {
          securityId: {
            dir: oracledb.BIND_IN,
            val: securityId,
            type: oracledb.INTEGER,
          },
        },
      )
      security.is_mainstock = entry.rows[0][0]
    }

    {
      // Add prices period to security.
      const entry = await connection.execute(
        `
          select
          to_char(min(prices_from), 'YYYY-MM-DD') as prices_from,
          to_char(max(prices_to), 'YYYY-MM-DD') as prices_to
          from
          (
              select min(day) as prices_from, max(day) as prices_to
              from notation_extra_price nep
              join notation n on (nep.notation = n.id)
              where n.stock = :securityId

              union

              select min(day) as prices_from, max(day) as prices_to
              from notation_price np
              join notation n on (np.notation = n.id)
              where n.stock = :securityId
          )
          `,
        {
          securityId: {
            dir: oracledb.BIND_IN,
            val: securityId,
            type: oracledb.INTEGER,
          },
        },
      )
      security.prices_period = entry.rows[0]
    }

    // Add security exchanges and notations to security.
    const notations = objectsFromSqlResult(
      await connection.execute(
        `
          select
            n.id,
            n.sector as sector_id,
            to_char(n.startdate, 'YYYY-MM-DD') as start_date,
            to_char(n.enddate, 'YYYY-MM-DD') as end_date,
            n.stockexchange_id as stock_exchange_id
          from notation n
          where n.stock = :securityId
          and n.sector <> 75
          order by n.startdate, n.enddate
        `,
        {
          securityId: {
            dir: oracledb.BIND_IN,
            val: securityId,
            type: oracledb.INTEGER,
          },
        },
      ),
    )
    {
      let notationByIdByStockExchangeId = {}
      for (let notation of notations) {
        if (notation.stock_exchange_id === null) {
          console.log(
            `Null stock exchange for notation ${notation.id} of security ${securityId}`,
          )
          continue
        }
        let notationById =
          notationByIdByStockExchangeId[notation.stock_exchange_id]
        if (notationById === undefined) {
          notationById = notationByIdByStockExchangeId[
            notation.stock_exchange_id
          ] = {}
        }
        notationById[notation.id] = notation
        delete notation.stock_exchange_id
      }
      security.notationByIdByStockExchangeId = notationByIdByStockExchangeId
    }

    {
      // Add stock_name.name to security notations names.
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              sn.name,
              to_char(sn.startdate, 'YYYY-MM-DD') as start_date,
              to_char(sn.enddate, 'YYYY-MM-DD') as end_date,
              sn.stockexchange as stock_exchange_id
            from stock_name sn
            where sn.stock = :securityId
              and sn.name <> 'STOCK_FICTIF'
          `,
          {
            securityId: {
              dir: oracledb.BIND_IN,
              val: securityId,
              type: oracledb.INTEGER,
            },
          },
        ),
      )
      for (let entry of entries) {
        entry.name = cleanUpLine(entry.name)
        const notationById =
          security.notationByIdByStockExchangeId[entry.stock_exchange_id]
        if (notationById === undefined) {
          console.log(
            `Unknown stock exchange "${entry.stock_exchange_id}" for security name "${entry.name}" of security ${securityId}`,
          )
          continue
        }
        delete entry.stock_exchange_id
        for (let notation of Object.values(notationById)) {
          const endDate =
            entry.end_date === null
              ? notation.end_date
              : notation.end_date === null
              ? entry.end_date
              : new Date(
                  Math.max(
                    new Date(entry.end_date),
                    new Date(notation.end_date),
                  ),
                )
                  .toISOString()
                  .split("T")[0]
          const startDate =
            entry.start_date === null
              ? notation.start_date
              : notation.start_date === null
              ? entry.start_date
              : new Date(
                  Math.min(
                    new Date(entry.start_date),
                    new Date(notation.start_date),
                  ),
                )
                  .toISOString()
                  .split("T")[0]
          if (startDate > endDate) {
            continue
          }
          let names = notation.names
          if (names === undefined) {
            names = notation.names = []
          }
          names.push({
            ...entry,
            start_date: startDate,
            end_date: endDate,
          })
        }
      }
    }

    // Compute best security name.
    const names = new Set()
    for (let notationById of Object.values(
      security.notationByIdByStockExchangeId,
    )) {
      for (let notation of Object.values(notationById)) {
        for (let name of notation.names || []) {
          names.add(name.name)
        }
      }
    }
    {
      let bestName = null
      let minDistance = Number.MAX_SAFE_INTEGER
      for (let name of names) {
        let distance = 0
        for (let otherName of names) {
          distance += levenshtein(name, otherName)
        }
        if (
          distance < minDistance ||
          (distance === minDistance && name.length < bestName.length)
        ) {
          bestName = name
          minDistance = distance
        }
      }
      security.name = bestName
    }

    securityItemId = securityItemIdById[securityId] = await upsertSecurity(
      connection,
      wikibase,
      security,
    )
  }
  return securityItemId
}
