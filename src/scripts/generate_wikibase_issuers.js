import commandLineArgs from "command-line-args"
import rpn from "request-promise-native"

import pkg from "../../package.json"
import config from "../config"
import { createPool } from "../database"
import { upsertIssuers } from "../issuers"
import Wikibase from "../wikibase"

const optionDefinitions = [
  { name: "issuer", type: Number, help: "ID of issuer to start resume from" },
]
const options = commandLineArgs(optionDefinitions)

async function main(pool) {
  const connection = await pool.getConnection()
  try {
    const request = rpn.defaults({
      headers: {
        "User-Agent": `${pkg.name}/${pkg.version} (${pkg.repository.url}; ${pkg.author})`,
      },
      jar: true,
    })

    const wikibase = new Wikibase({
      ...config.wikibase,
      request,
    })
    await wikibase.login()
    await wikibase.requestCsrfToken()

    await upsertIssuers(connection, wikibase, { firstId: options.issuer})
  } finally {
    connection.close()
  }
}

createPool()
  .then(pool => {
    main(pool).catch(error => {
      console.log(error.stack || error)
      process.exit(1)
    })
  })
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
